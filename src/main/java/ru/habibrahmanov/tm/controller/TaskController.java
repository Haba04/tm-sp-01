package ru.habibrahmanov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.enumeration.Status;

import java.util.Arrays;
import java.util.List;

@Controller
public class TaskController extends AbstractController {

    @RequestMapping(value = "/taskCreate/{projectId}", method = RequestMethod.POST)
    public String createTaskPost(
            @PathVariable final String projectId,
            @ModelAttribute("name") final String name,
            @ModelAttribute("description") final String description,
            @ModelAttribute("dateBegin") final String dateBegin,
            @ModelAttribute("dateEnd") final String dateEnd
    ) throws Exception {
        taskService.insert(projectService.findOne(projectId), name, description, dateBegin, dateEnd);
        return "redirect:/tasks/{projectId}";
    }

    @RequestMapping(value = "/taskCreate/{projectId}", method = RequestMethod.GET)
    public String createTaskGet(@PathVariable final String projectId, @ModelAttribute("task") final Task task) {
        task.setProject(projectService.findOne(projectId));
        return "taskCreate";
    }

    @RequestMapping(value = "/tasks/{projectId}", method = RequestMethod.GET)
    public String showAll(@NotNull final Model model, @PathVariable final String projectId) {
        List<Task> taskList = taskService.findAll(projectId);
        model.addAttribute("taskList", taskList);
        return "task";
    }

    @RequestMapping(value = "/taskRemove/{taskId}", method = RequestMethod.GET)
    public String removeOne(@PathVariable final String taskId) {
        taskService.remove(taskId);
        return "redirect:/projects";
    }

    @GetMapping(value = "/taskEdit/{id}")
    public String taskEditGet(final Model model, @PathVariable final String id) {
        model.addAttribute("task", taskService.findOne(id));
        model.addAttribute("status", Arrays.asList(Status.values()));
        return "taskEdit";
    }

    @PostMapping(value = "/taskEdit/{id}")
    public String taskEditPost(
            @PathVariable final String id,
            @ModelAttribute("name") final String name,
            @ModelAttribute("description") final String description,
            @ModelAttribute("status") final Status status,
            @ModelAttribute("dateBegin") final String dateBegin,
            @ModelAttribute("dateEnd") final String dateEnd
    ) throws Exception {
        taskService.update(id, name, description, status, dateBegin, dateEnd);
        return "redirect:/projects";
    }
}
