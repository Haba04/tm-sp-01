<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12.02.2020
  Time: 14:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a style="font-size: 26px" class="navbar-brand mb-0 h1" href="/">PROJECT_MANAGER</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div style="margin-left: 30px" class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a style="font-size: 20px" class="nav-link" href="/">HOME</a>
            </li>
            <li class="nav-item">
                <a style="font-size: 20px; margin-left: 10px" class="nav-link" href="/projects">PROJECTS</a>
            </li>
        </ul>
    </div>
</nav>
</body>
</html>