<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12.02.2020
  Time: 11:00
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
<%--    <%@include file="navbar.jsp" %>--%>
<%--    <title>Project view</title>--%>
<%--</head>--%>
<%--<body>--%>
<%--<table>--%>
<%--    <tbody>--%>
<%--    <tr>--%>
<%--        <td>id</td>--%>
<%--        <td>${project.id}</td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--        <td style="width: 30%;">name</td>--%>
<%--        <td style="width: 70%;">${project.name}</td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--        <td style="width: 30%;">description</td>--%>
<%--        <td style="width: 70%;">${project.description}</td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--        <td style="width: 30%;">Date begin</td>--%>
<%--        <td style="width: 70%;">${project.dateBegin}</td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--        <td style="width: 30%;">Date end</td>--%>
<%--        <td style="width: 70%;">${project.dateEnd}</td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--        <td style="width: 30%;">status</td>--%>
<%--        <td style="width: 70%;">${project.status}</td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--        <td style="width: 30%;">tasks</td>--%>
<%--        <td style="width: 70%;">--%>
<%--            <c:forEach var="task" items="${tasks}">--%>
<%--                ${task.name}<br>--%>
<%--            </c:forEach>--%>
<%--        </td>--%>
<%--    </tr>--%>
<%--    </tbody>--%>
<%--</table>--%>
<%--</body>--%>
<%--</html>--%>



<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Project</title>
</head>
<%@include file="navbar.jsp" %>
<body>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>Project id</th>
        <th>Project name</th>
        <th>Project description</th>
        <th>Project date begin</th>
        <th>Project date end</th>
        <th>Project status</th>
        <th>Tasks</th>
        <th>Edit</th>
        <th>Remove</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><c:out value="${project.id}"/></td>
        <td><c:out value="${project.name}"/></td>
        <td><c:out value="${project.description}"/></td>
        <td><c:out value="${project.dateBegin}"/></td>
        <td><c:out value="${project.dateEnd}"/></td>
        <td><c:out value="${project.status}"/></td>
        <td><a href="${pageContext.request.contextPath}/tasks/${project.id}">Tasks</a></td>
        <td><a href="${pageContext.request.contextPath}/projectEdit/${project.id}">Edit</a></td>
        <td><a href="${pageContext.request.contextPath}/projectRemove/${project.id}">Remove</a></td>
    </tr>
    </tbody>
</table>
</body>
</html>